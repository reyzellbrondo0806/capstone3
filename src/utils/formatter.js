export function formatPrice({ price = 0, format, withoutCents }) {
	return price.toLocaleString('en-US', {
		style: 'currency',
		currency: format,
		minimumFractionDigits: withoutCents ? 0 : 2,
		maximumFractionDigits: withoutCents ? 0 : 2,
	});
}
