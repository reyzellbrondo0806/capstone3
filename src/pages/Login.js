import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { isEmpty } from 'lodash-es';
import axios from 'axios';
import LoopOutlinedIcon from '@mui/icons-material/LoopOutlined';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';
import { API_BASE_ROUTE } from '../components/constants/main';

export default function Login() {
	const { setUser, setUserProducts, user } = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [formData, setFormData] = useState({
		email: '',
		password: '',
	});

	const { email, password } = formData;

	// State to determine whether submit button is enabled or not
	const [disableButton, setDisableButton] = useState(true);
	const [loading, setLoading] = useState(false);
	const [showPassword, setShowPassword] = useState(false);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if (isFormValid()) {
			setDisableButton(false);
		} else {
			setDisableButton(true);
		}
	}, [formData]);

	function isFormValid() {
		if (isEmpty(email) || isEmpty(password)) {
			return false;
		}

		return true;
	}

	function fetchUserOrders(user) {
		axios
			.get(`${API_BASE_ROUTE}/orders/userOrder`, {
				params: { userId: user.id }, // Pass the user ID in the request body
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${user.access}`, // Replace with your authorization token
				},
			})
			.then((res) => {
				const { data } = res;
				setUserProducts(data);
				navigate('/');
			})
			.catch((err) => {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Uh oh, Something went wrong',
				});
			});
	}

	function onSuccess(data) {
		if (data) {
			// Set the email of the authenticated user in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value);
			localStorage.setItem('token', data.access);
			setUser({
				id: data.id,
				isAdmin: data.isAdmin,
				access: data.access,
			});

			setFormData({
				email: '',
				password: '',
			});

			Swal.fire({
				title: 'Login Successful',
				icon: 'success',
				text: 'Welcome to PowerMac!',
			});
		} else {
			Swal.fire({
				title: 'Authentication failed',
				icon: 'error',
				text: 'Check your login details and try again.',
			});
			setLoading(false);
			setDisableButton(false);
		}
	}

	function submit(e) {
		setLoading(true);
		setDisableButton(true);
		// Prevents page redirection via form submission
		e.preventDefault();
		axios
			.post(`${API_BASE_ROUTE}/users/login`, formData)
			.then((res) => {
				const { data } = res;
				fetchUserOrders(data);
				onSuccess(data);
			})
			.catch((error) => {
				console.log(error);
				Swal.fire({
					title: 'Authentication failed',
					icon: 'error',
					text: 'Check your login details and try again.',
				});
				setLoading(false);
				setDisableButton(false);
			});
	}

	function handleChange(e) {
		const { name, value } = e.target;

		setFormData({
			...formData,
			[name]: value,
		});
	}

	function renderShowPassword() {
		if (showPassword) {
			return (
				<RemoveRedEyeOutlinedIcon
					className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
					fontSize="small"
					onClick={() => setShowPassword(false)}
				/>
			);
		}

		return (
			<VisibilityOffOutlinedIcon
				className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
				fontSize="small"
				onClick={() => setShowPassword(true)}
			/>
		);
	}

	return (
		<div className="w-screen flex items-center justify-center my-20 text-center">
			<div className="w-2/3 md:w-1/2 md:px-6">
				<h1 className="text-5xl font-semibold mb-10">Login</h1>
				<form onSubmit={(e) => submit(e)}>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type="text"
							className=" outline-none appearance-none w-full h-full text-lg"
							name="email"
							value={email}
							onChange={handleChange}
							placeholder="Email"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-1">
						<input
							type={showPassword ? 'text' : 'password'}
							name="password"
							placeholder="Password"
							value={password}
							onChange={handleChange}
							className=" outline-none appearance-none w-full h-full text-lg"
						/>
					</div>
					<div className="w-full text-left mb-10">{renderShowPassword()}</div>
					<button
						type="submit"
						className={`${
							disableButton
								? 'bg-gray-500'
								: 'bg-blue-500 hover:bg-blue-300 hover:cursor-pointer'
						} rounded-full text-white px-10 py-3 font-semibold text-lg`}
						disabled={disableButton}
					>
						{loading ? (
							<LoopOutlinedIcon className="animate-spin" />
						) : (
							'Sign in'
						)}
					</button>
				</form>

				<div
					className="text-blue-500 hover:text-blue-300 hover:underline hover:cursor-pointer mt-4"
					onClick={() => navigate('/register')}
				>
					Create account
				</div>
			</div>
		</div>
	);
}
