import { useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Products() {
	const { user } = useContext(UserContext);
	const isAdmin = user?.isAdmin;

	return isAdmin ? <AdminView /> : <UserView />;
}
