import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Logout() {
	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);
	// Placing the "setUser" setter function inside of a useEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render a different component
	// By adding the useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user
	useEffect(() => {
		// Set the user state back to it's original value
		setUser({
			id: null,
			isAdmin: null,
			access: null,
		});

		unsetUser();

		Swal.fire({
			title: 'Signed out',
			icon: 'success',
			text: 'You have successfully signed out.',
		});
	}, []);

	// Navigate back to login
	return <Navigate to="/login" />;
}
