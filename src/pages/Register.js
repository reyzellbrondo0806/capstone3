import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { isEmpty, isNil } from 'lodash-es';
import axios from 'axios';
import LoopOutlinedIcon from '@mui/icons-material/LoopOutlined';
import Swal from 'sweetalert2';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';
import { API_BASE_ROUTE } from '../components/constants/main';

export default function Register() {
	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [formData, setFormData] = useState({
		firstName: '',
		lastName: '',
		email: '',
		mobileNo: '',
		password: '',
		confirmPassword: '',
	});
	// Destructure formData
	const { firstName, lastName, email, mobileNo, password, confirmPassword } =
		formData;
	// State to determine whether submit button is enabled or not
	const [disableButton, setDisableButton] = useState(true);
	const [loading, setLoading] = useState(false);
	const [showPassword, setShowPassword] = useState(false);

	useEffect(() => {
		if (isFormValid()) {
			setDisableButton(false);
		} else {
			setDisableButton(true);
		}
	}, [formData]);

	useEffect(() => {
		if (!isNil(user?.id)) {
			navigate('/');
		}
	}, []);

	function submit(e) {
		// Prevents page redirection via form submission
		e.preventDefault();
		const startsWith09Pattern = /^09\d*/;

		if (!startsWith09Pattern.test(mobileNo)) {
			return alert('Mobile no must start with -09-');
		}

		if (!isFormValid())
			return Swal.fire({
				title: 'Invalid fields',
				icon: 'warning',
				text: 'All required fields should be present',
			});

		setLoading(true);
		setDisableButton(true);
		axios
			.post(`${API_BASE_ROUTE}/users/register`, formData)
			.then((res) => {
				setFormData({
					firstName: '',
					lastName: '',
					email: '',
					mobileNo: '',
					password: '',
					confirmPassword: '',
				});
				Swal.fire({
					title: 'Registration Success!',
					icon: 'success',
					text: 'Login to continue',
				});
				navigate('/login');
				setLoading(false);
				setDisableButton(false);
			})
			.catch(() => {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Uh, Oh something went wrong',
				});
				setDisableButton(true);
				setLoading(false);
			});
	}

	// Validation checking if any of the values is Empty
	function isFormValid() {
		if (
			isEmpty(firstName) ||
			isEmpty(lastName) ||
			isEmpty(email) ||
			isEmpty(mobileNo) ||
			isEmpty(password) ||
			isEmpty(confirmPassword) ||
			password !== confirmPassword
		) {
			return false;
		}

		return true;
	}

	function handleChange(e) {
		const { name, value } = e.target;
		// Define a regex pattern to match non-numerical characters
		const nonNumericPattern = /[^0-9]/g;

		if (name === 'mobileNo') {
			// We will return if the user tried to type a character
			// on mobileNo input
			if (nonNumericPattern.test(value)) return;
		}

		setFormData({
			...formData,
			[name]: value,
		});
	}

	function renderShowPassword() {
		if (showPassword) {
			return (
				<RemoveRedEyeOutlinedIcon
					className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
					fontSize="small"
					onClick={() => setShowPassword(false)}
				/>
			);
		}

		return (
			<VisibilityOffOutlinedIcon
				className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
				fontSize="small"
				onClick={() => setShowPassword(true)}
			/>
		);
	}

	return (
		<div className="w-screen flex items-center justify-center my-20 text-center">
			<div className="w-2/3 md:w-1/2 md:px-6">
				<h1 className="text-5xl font-semibold mb-10">Create Account</h1>
				<form onSubmit={(e) => submit(e)}>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type="text"
							className=" outline-none appearance-none w-full h-full text-lg"
							name="firstName"
							value={firstName}
							onChange={handleChange}
							placeholder="First name"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type="text"
							className=" outline-none appearance-none w-full h-full text-lg"
							name="lastName"
							value={lastName}
							onChange={handleChange}
							placeholder="Last name"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type="email"
							className=" outline-none appearance-none w-full h-full text-lg"
							name="email"
							value={email}
							onChange={handleChange}
							placeholder="Email"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type="text"
							pattern="\d*"
							maxLength={11}
							min={11}
							className="outline-none appearance-none w-full h-full text-lg"
							name="mobileNo"
							value={mobileNo}
							onChange={handleChange}
							placeholder="Mobile no"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-4">
						<input
							type={showPassword ? 'text' : 'password'}
							className=" outline-none appearance-none w-full h-full text-lg"
							name="password"
							value={password}
							onChange={handleChange}
							placeholder="Password"
						/>
					</div>
					<div className="border-2 border-gray-400 hover:border-gray-600 rounded-lg px-6 py-4 mb-1">
						<input
							type={showPassword ? 'text' : 'password'}
							className=" outline-none appearance-none w-full h-full text-lg"
							name="confirmPassword"
							value={confirmPassword}
							onChange={handleChange}
							placeholder="Confirm password"
						/>
					</div>
					<div className="w-full text-left mb-4">{renderShowPassword()}</div>
					<button
						type="submit"
						className={`${
							disableButton
								? 'bg-gray-500'
								: 'bg-blue-500 hover:bg-blue-300 hover:cursor-pointer'
						} rounded-full text-white px-10 py-3 font-semibold text-lg`}
						disabled={disableButton}
					>
						{loading ? <LoopOutlinedIcon className="animate-spin" /> : 'Create'}
					</button>
				</form>
			</div>
		</div>
	);
}
