//import FeaturedProducts from '../components/FeaturedProducts';
import { Pagination, A11y, Autoplay, Navigation } from 'swiper/modules';
import { useState, useEffect } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import { formatPrice } from '../utils/formatter';

export default function Home() {
	const [allProductsSlidesPerView, setAllProductsSlidesPerView] = useState(2);
	const [newProductSlidesPerView, setNewProductSlidesPerView] = useState(4);

	useEffect(() => {
		// Check the window width and set the number of slides per view accordingly
		const handleResize = () => {
			if (window.innerWidth < 768) {
				setNewProductSlidesPerView(2);
				setAllProductsSlidesPerView(2); // Set to 2 on mobile screens
			} else {
				setNewProductSlidesPerView(4);
				setAllProductsSlidesPerView(6); // Set to 8 on larger screens
			}
		};

		// Initialize with the correct value on component mount
		handleResize();

		// Add a resize event listener to update the value when the window size changes
		window.addEventListener('resize', handleResize);

		// Clean up the event listener on component unmount
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);

	function renderAllProductsSection() {
		return (
			<section className="w-full py-8 border-t-2 border-b-2 border-gray-300">
				<h1 className="mx-6 font-bold text-2xl mb-6">
					View all Apple products.
				</h1>
				<Swiper
					modules={[Navigation]}
					slidesPerView={allProductsSlidesPerView}
					navigation={true}
					spaceBetween={10}
					className="relative px-4"
				>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">Mac</h1>
						<p>
							From{' '}
							{formatPrice({ price: 39000, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">iPad</h1>
						<p>
							From{' '}
							{formatPrice({ price: 25490, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">iPhone</h1>
						<p>
							From{' '}
							{formatPrice({ price: 34490, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">Watch</h1>
						<p>
							From{' '}
							{formatPrice({ price: 16990, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">Music</h1>
						<p>
							From{' '}
							{formatPrice({ price: 8990, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">TV & Home</h1>
						<p>
							From{' '}
							{formatPrice({ price: 10490, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">Accessories</h1>
						<p>
							From{' '}
							{formatPrice({ price: 1390, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col items-center">
						<div>IMAGE</div>
						<h1 className="font-bold">AirTag</h1>
						<p>
							From{' '}
							{formatPrice({ price: 1890, format: 'PHP', withoutCents: true })}
						</p>
					</SwiperSlide>
				</Swiper>
			</section>
		);
	}

	return (
		<>
			<Swiper
				modules={[Pagination, A11y, Autoplay]}
				autoplay={{
					delay: 2500,
					disableOnInteraction: false,
				}}
				pagination={{ clickable: true }}
			>
				<SwiperSlide className="w-full flex items-center justify-center">
					<img
						src="//powermaccenter.com/cdn/shop/files/MacBook_Air_15_-_Desktop_2b1d78f6-59fc-42e4-a151-95a428fbdff3_1500x.png?v=1694162383"
						alt="mac"
						className="h-full w-2/3"
					/>
				</SwiperSlide>
				<SwiperSlide className="w-full flex items-center justify-center bg-neutral-100">
					<img
						src="//powermaccenter.com/cdn/shop/files/PD914___eCommerce_Desktop_1500x.gif?v=1694745300"
						alt="phone"
						className="h-full md:w-2/3"
					/>
				</SwiperSlide>
				<SwiperSlide className="w-full flex items-center justify-center">
					<img
						src="//powermaccenter.com/cdn/shop/files/iFA-Homepage-Desk_1500x.png?v=1694170310"
						alt="phone-1"
						className="h-full md:w-2/3"
					/>
				</SwiperSlide>
				<SwiperSlide className="w-full flex items-center justify-center">
					<img
						src="//powermaccenter.com/cdn/shop/files/Mac_Me_-_Desktop_1d0af4ce-00b4-4689-b68e-827a7d65e77c_1500x.png?v=1694170190"
						alt="phone-2"
						className="h-full md:w-2/3"
					/>
				</SwiperSlide>
				<SwiperSlide className="w-full flex items-center justify-center bg-neutral-400">
					<img
						src="//powermaccenter.com/cdn/shop/files/BOMBelkin-DesktopBanner_1500x.png?v=1694761091"
						alt="phone-3"
						className="h-full md:w-2/3"
					/>
				</SwiperSlide>
			</Swiper>
			{renderAllProductsSection()}
			<section className="w-full py-8 border-b-2 border-gray-300">
				<h1 className="font-bold text-2xl mb-6 text-gray-900 mx-6">
					See whats new.
				</h1>
				<Swiper
					modules={[Navigation]}
					slidesPerView={newProductSlidesPerView}
					navigation={true}
					spaceBetween={10}
					className="relative px-4"
				>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
					<SwiperSlide className="flex flex-col text-left gap-2 p-4 shadow-lg">
						<div>IMAGE</div>
						<p className="text-sm text-orange-400">NEW</p>
						<h1 className="text-2xl text-gray-900 font-bold">MacBook Air</h1>
						<p className="mb-4">Imressively big. Impossibly Thin.</p>
						<p>
							Price starts at{' '}
							{formatPrice({ price: 89990, format: 'PHP', withoutCents: true })}{' '}
							or{' '}
							{formatPrice({
								price: 3749.58,
								format: 'PHP',
								withoutCents: true,
							})}{' '}
							in 24 easy payments
						</p>
					</SwiperSlide>
				</Swiper>
			</section>
		</>
	);
}
