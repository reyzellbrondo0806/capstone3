import { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import axios from 'axios';
import Loading from '../components/loaders/Loading';
import { formatPrice } from '../utils/formatter';
import { API_BASE_ROUTE } from '../components/constants/main';

export default function ProductView() {
	const { productId } = useParams();
	const { setProduct, setShowBuyButton, product } = useContext(UserContext);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		fetchProductDetails();
		setShowBuyButton(true);
		return () => {
			setShowBuyButton(false);
		};
	}, []);

	function fetchProductDetails() {
		setLoading(true);
		axios
			.get(`${API_BASE_ROUTE}/products/${productId}`)
			.then((res) => {
				const { data } = res;

				setProduct(data);
			})
			.catch((err) => {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.',
				});
			})
			.finally(() => {
				setLoading(false);
			});
	}

	if (loading) {
		return <Loading />;
	}

	return (
		<section className="w-screen relative h-full">
			<div className="w-full flex flex-row px-4 py-8 items-center justify-center gap-6">
				<div className="w-full">IMAGE</div>
				<div className="flex flex-col text-left w-full">
					<h1 className="text-black font-semibold text-2xl">{product.name}</h1>
					<p className="text-gray-500 my-6">{product.description}</p>
					<p className="text-black text-2xl font-semibold">
						{formatPrice({
							price: product.price,
							format: 'PHP',
							withoutCents: true,
						})}
					</p>
				</div>
			</div>
		</section>
	);
}
