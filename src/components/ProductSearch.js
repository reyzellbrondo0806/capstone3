import React, { useState } from 'react';
import ProductCard from './ProductCard';
import axios from 'axios';
import { API_BASE_ROUTE } from './constants/main';

const ProductSearch = ({ setHasSearchResult }) => {
	const [searchQuery, setSearchQuery] = useState('');
	const [searchResults, setSearchResults] = useState([]);
	const [loading, setIsLoading] = useState(false);

	function handleSearch() {
		setIsLoading(true);

		axios
			.post(`${API_BASE_ROUTE}/products/searchByName`, {
				name: searchQuery,
			})
			.then((res) => {
				const { data } = res;
				setSearchResults(data);
				setHasSearchResult(true);
			})
			.catch((error) => {
				console.log(error);
			})
			.finally(() => {
				setIsLoading(false);
			});
	}

	if (loading) {
		return <h1>Loading...</h1>;
	}

	return (
		<div className="pt-5 container">
			<h2>Product Search</h2>
			<div className="form-group">
				<label htmlFor="productName">Product Name:</label>
				<input
					type="text"
					id="productName"
					className="form-control"
					value={searchQuery}
					onChange={(event) => setSearchQuery(event.target.value)}
				/>
			</div>
			<button className="btn btn-primary my-4" onClick={handleSearch}>
				Search
			</button>
			<h3>Search Results:</h3>
			<ul>
				{searchResults?.map((product) => (
					//   <li key={course._id}>{course.name}</li>
					<ProductCard product={product} />
				))}
			</ul>
		</div>
	);
};

export default ProductSearch;
