import React, { useContext, useEffect, useState } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../../UserContext';
import { isNil } from 'lodash-es';
import powerMacImage from '../../assets/images/powermacLogo.jpg';
import SearchIcon from '@mui/icons-material/Search';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import LocalMallOutlinedIcon from '@mui/icons-material/LocalMallOutlined';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import AddCircleOutlinedIcon from '@mui/icons-material/AddCircleOutlined';
import { BASE_ROUTE } from '../constants/main';

export default function AppNavbar() {
	const {
		user,
		setSearchTermValue,
		setMenuOpen,
		menuOpen,
		hideTopBar,
		setShowProductModal,
		userProducts,
	} = useContext(UserContext);
	const [searchTerm, setSearchTerm] = useState('');
	const navigate = useNavigate();

	function renderLoginLogout() {
		if (!isNil(user?.id)) {
			return (
				<LogoutOutlinedIcon
					className="text-gray-500 hover:cursor-pointer hover:text-blue-500"
					fontSize="large"
					onClick={() => navigate('/logout')}
				/>
			);
		}

		return (
			<PersonOutlineOutlinedIcon
				className="text-gray-500 hover:cursor-pointer hover:text-blue-500"
				fontSize="large"
				onClick={() => navigate('/login')}
			/>
		);
	}

	return (
		<nav
			className={`bg-white text-black sticky w-full top-0 left-0 border-b-2 border-gray-300 flex flex-col ${
				hideTopBar ? 'hidden' : 'z-20'
			}`}
		>
			<div className="w-full flex flex-wrap items-center justify-between mx-auto py-4 px-6">
				<img
					src={powerMacImage}
					alt="powermac"
					className="h-20 hover:cursor-pointer order-2 md:order-1"
					onClick={() => navigate('/')}
				/>
				<div className="flex order-1 md:order-2 items-center gap-2">
					<span className="hidden md:block">{renderLoginLogout()}</span>
					{user.isAdmin ? (
						<span className="hidden md:block">
							<AddCircleOutlinedIcon
								className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
								fontSize="large"
								onClick={() => setShowProductModal(true)}
							/>
						</span>
					) : (
						<span className="hidden md:block relative">
							<LocalMallOutlinedIcon
								className="text-gray-500 hover:text-blue-500 hover:cursor-pointer"
								fontSize="large"
							/>
							{userProducts?.orders?.length > 0 ? (
								<span className="absolute -top-1 -right-1 text-white font-bold text-xs bg-gray-500 py-1 px-2 rounded-full animate-pulse">
									{userProducts?.orders?.length}
								</span>
							) : null}
						</span>
					)}

					<button
						type="button"
						className="h-10 w-10 border-0 focus:ring-1 focus:ring-gray-500 flex items-center justify-center focus:rounded-lg md:hidden"
						onClick={() => setMenuOpen((prev) => !prev)}
					>
						{menuOpen ? (
							<CloseIcon className="text-gray-500 fade-animation" />
						) : (
							<MenuIcon className="text-gray-500 fade-animation" />
						)}
					</button>
				</div>
				<div className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1">
					<div className="w-[500px] border border-gray-300 rounded-full flex flex-row p-2 items-center bg-gray-200">
						<SearchIcon
							className="w-2/6 text-gray-500 hover:text-blue-700 hover:cursor-pointer"
							onClick={() => setSearchTermValue(searchTerm)}
						/>
						<input
							type="text"
							placeholder="Search"
							className="w-4/6 h-full outline-none bg-gray-200"
							value={searchTerm}
							onChange={(e) => setSearchTerm(e.target.value)}
							onKeyDown={(e) => {
								if (e.key === 'Enter') {
									setSearchTermValue(searchTerm);
								}
							}}
						/>
					</div>
				</div>
			</div>
			<div className="w-full md:flex flex-row justify-around text-gray-900 text-lg py-4 hidden">
				<button onClick={() => navigate('/products')}>Mac</button>
				<button onClick={() => navigate('/products')}>iPad</button>
				<button onClick={() => navigate('/products')}>iPhone</button>
				<button onClick={() => navigate('/products')}>Watch</button>
				<button onClick={() => navigate('/products')}>Music</button>
				<button onClick={() => navigate('/products')}>TV & Home</button>
				<button onClick={() => navigate('/products')}>Accessories</button>
			</div>
		</nav>
	);
}
