import RestartAltOutlinedIcon from '@mui/icons-material/RestartAltOutlined';
import React, { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';

export default function Loading() {
	const { setHideTopBar } = useContext(UserContext);

	useEffect(() => {
		// On mount we hide the topbar
		setHideTopBar(true);

		// On unmount we set it to default value
		return () => {
			setHideTopBar(false);
		};
	}, []);
	return (
		<div className="w-screen absolute z-50 h-screen flex bg-black bg-opacity-90 items-center justify-center text-white">
			<RestartAltOutlinedIcon
				className="text-white animate-spin"
				fontSize="large"
			/>
		</div>
	);
}
