import { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import UserContext from '../UserContext';
import { isNil } from 'lodash-es';
import Loading from './loaders/Loading';
import { formatPrice } from '../utils/formatter';
import ArrowForwardIosOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined';
import ArrowBackIosOutlinedIcon from '@mui/icons-material/ArrowBackIosOutlined';
import Swal from 'sweetalert2';
import { API_BASE_ROUTE } from './constants/main';

export default function AdminView() {
	const [productLoading, setProductLoading] = useState(false);
	const [products, setProducts] = useState([]);
	const {
		user,
		setShowProductModal,
		setProduct,
		productSaved,
		setProductSaved,
	} = useContext(UserContext);
	const [currentPage, setCurrentPage] = useState(1);
	const [lastPage, setLastPage] = useState(0);

	useEffect(() => {
		if (!user?.isAdmin) return;
		fetchProducts();
		setCurrentPage(1);
	}, []);

	useEffect(() => {
		fetchProducts();
	}, [currentPage]);

	useEffect(() => {
		if (productSaved) {
			fetchProducts();
		} else {
			setProductSaved(false);
		}
	}, [productSaved]);

	if (productLoading) {
		return <Loading />;
	}

	function fetchProducts() {
		setProductLoading(true);
		axios
			.get(`${API_BASE_ROUTE}/products/allProducts?page=${currentPage}`, {
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((res) => {
				const { data } = res;
				console.log(data);
				setProducts(data.products);
				setLastPage(parseInt(data.totalPages));
			})
			.catch((error) => {
				console.log('Error fetching products data', error);
				setProductLoading(false);
			})
			.finally(() => {
				setProductLoading(false);
			});
	}

	function handleEdit(product) {
		setProduct(product);
		setShowProductModal(true);
	}

	function archiveToggle(product) {
		setProductLoading(true);
		axios
			.put(`${API_BASE_ROUTE}/products/${product?._id}/archive`)
			.then((res) => {
				const { data } = res;

				if (data) {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: `${product?.name} was successfully archived`,
					});
					fetchProducts();
				} else {
					Swal.fire({
						title: 'Something Went Wrong',
						icon: 'error',
						text: 'Please Try again',
					});
				}
			})
			.catch(() => {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again',
				});
				setProductLoading(false);
			})
			.finally(() => {
				setProductLoading(false);
			});
	}

	function activateToggle(product) {
		setProductLoading(true);
		axios
			.put(`${API_BASE_ROUTE}/products/${product?._id}/activate`)
			.then((res) => {
				const { data } = res;

				if (data) {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: `${product?.name} was sucessfully activated`,
					});
					fetchProducts();
				} else {
					Swal.fire({
						title: 'Something Went Wrong',
						icon: 'error',
						text: 'Please Try again',
					});
				}
			})
			.catch(() => {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again',
				});
				setProductLoading(false);
			})
			.finally(() => {
				setProductLoading(false);
			});
	}

	function handleToggle(product) {
		axios.defaults.headers.common[
			'Authorization'
		] = `Bearer ${localStorage.getItem('token')}`;
		if (product.isActive) {
			archiveToggle(product);
		} else {
			activateToggle(product);
		}
	}

	function renderPaginationButtons() {
		return (
			<div className="w-full flex flex-row justify-between items-center py-4 px-3">
				<button
					className={`${
						currentPage === 1
							? 'text-gray-500 hover:cursor-not-allowed'
							: 'text-blue-700'
					}`}
					onClick={() => {
						if (currentPage !== 1) {
							setCurrentPage(currentPage - 1);
						}
					}}
				>
					<ArrowBackIosOutlinedIcon />
				</button>
				<button
					className={`${
						lastPage > currentPage
							? 'text-blue-700'
							: 'text-gray-500 hover:cursor-not-allowed'
					}`}
					onClick={() => {
						console.log(lastPage);
						console.log(currentPage);
						if (lastPage > currentPage) {
							setCurrentPage(currentPage + 1);
						}
					}}
				>
					<ArrowForwardIosOutlinedIcon />
				</button>
			</div>
		);
	}

	return (
		<section>
			<div className="relative overflow-x-auto">
				<table className="w-full text-sm text-left text-gray-500 shadow-sm rounded-lg">
					<thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
						<tr>
							<th scope="col" className="px-6 py-3">
								Product name
							</th>
							<th scope="col" className="px-6 py-3">
								Description
							</th>
							<th scope="col" className="px-6 py-3">
								Price
							</th>
							<th scope="col" className="px-6 py-3">
								Stocks
							</th>
							<th scope="col" className="px-6 py-3">
								Availability
							</th>
							<th scope="col" className="px-6 py-3">
								Status
							</th>
						</tr>
					</thead>
					<tbody>
						{products.map((product) => {
							return (
								<tr key={product._id} className="border border-gray-500">
									<th
										scope="row"
										className="px-6 py-4 font-medium text-gray-900"
									>
										{product.name}
									</th>
									<td className="px-6 py-4 text-gray-900">
										{product.description}
									</td>
									<td className="px-6 py-4 text-gray-900">
										{formatPrice({
											price: product?.price,
											format: 'PHP',
											withoutCents: true,
										})}
									</td>
									<td className="px-6 py-4 text-gray-900">
										{product?.stocks || 0}
									</td>
									<td
										className={`px-6 py-4 text-gray-900 ${
											product.isActive ? 'text-green-500' : 'text-red-500'
										}`}
									>
										{product.isActive ? 'Available' : 'Unavailable'}
									</td>
									<td className="px-6 py-4 flex flex-row gap-2 items-center">
										<h1
											className="text-blue-500 hover:text-blue-300 hover:cursor-pointer"
											onClick={() => handleEdit(product)}
										>
											Edit Product
										</h1>
										<button
											className={`px-4 py-2 rounded-full text-white ${
												product.isActive
													? 'bg-red-500 hover:bg-red-300'
													: 'bg-green-500 hover:bg-green-300'
											}`}
											onClick={() => handleToggle(product)}
										>
											{product.isActive ? 'Archive' : 'Active'}
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
				{renderPaginationButtons()}
			</div>
		</section>
	);
}
