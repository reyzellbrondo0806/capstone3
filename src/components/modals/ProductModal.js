import React, { useContext, useState } from 'react';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import UserContext from '../../UserContext';
import { formatPrice } from '../../utils/formatter';
import { isNil } from 'lodash-es';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router';
import LoopOutlinedIcon from '@mui/icons-material/LoopOutlined';
import { API_BASE_ROUTE } from '../constants/main';

export default function ProductModal() {
	const {
		setShowProductModal,
		product,
		user,
		setProductSaved,
		setUserProducts,
	} = useContext(UserContext);
	const [quantity, setQuantity] = useState(0);
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const [formData, setFormData] = useState({
		name: product?.name || '',
		description: product?.description || '',
		price: product?.price || '',
		stocks: product?.stocks || '',
	});
	const { name, description, price, stocks } = formData;

	function fetchUserOrders() {
		axios
			.get(`${API_BASE_ROUTE}/orders/userOrder`, {
				params: { userId: user.id }, // Pass the user ID in the request body
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`, // Replace with your authorization token
				},
			})
			.then((res) => {
				const { data } = res;
				setUserProducts(data);
			})
			.catch((err) => {
				console.log(err);
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Uh oh, Something went wrong',
				});
			});
	}

	function checkout() {
		if (quantity > parseInt(product?.stocks || 0)) {
			return;
		}
		setLoading(true);
		axios
			.post(
				`${API_BASE_ROUTE}/orders/checkout`,
				{
					productId: product?._id,
					quantity: quantity,
				},
				{
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`,
					},
				}
			)
			.then((res) => {
				const { data } = res;
				Swal.fire({
					title: 'Order placed!',
					icon: 'success',
				});
				setShowProductModal(false);
				navigate('/products');
			})
			.catch(() => {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.',
				});
			})
			.finally(() => {
				fetchUserOrders();
				setLoading(false);
			});
	}

	function editProduct() {
		setLoading(true);
		axios
			.put(`${API_BASE_ROUTE}/products/${product?._id}`, formData, {
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((res) => {
				const { data } = res;
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated',
				});
				setShowProductModal(false);
				setProductSaved(true);
			})
			.catch(() => {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again',
				});
			})
			.finally(() => {
				setLoading(false);
			});
	}

	function addProduct() {
		setLoading(true);
		axios
			.post(`${API_BASE_ROUTE}/products/addProduct`, formData, {
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((res) => {
				Swal.fire({
					icon: 'success',
					title: 'Product Added',
				});

				setFormData({
					name: '',
					description: '',
					price: '',
					stocks: '',
				});
				setShowProductModal(false);
				setProductSaved(true);
			})
			.catch(() => {
				Swal.fire({
					icon: 'error',
					title: 'Unsuccessful Product Creation',
				});
				setLoading(false);
			})
			.finally(() => {
				setLoading(false);
			});
	}

	function submit(e) {
		e.preventDefault();
		if (user?.isAdmin) {
			isNil(product?._id) ? addProduct() : editProduct();
		} else {
			checkout();
		}
	}

	function handleChange(e) {
		const { name, value } = e.target;

		setFormData({
			...formData,
			[name]: value,
		});
	}

	function renderMainContent() {
		const addProduct = isNil(product?._id);
		if (user?.isAdmin) {
			return (
				<form onSubmit={(e) => submit(e)} className="flex flex-col">
					<h1
						className={`font-bold text-2xl ${
							addProduct ? 'text-green-500' : 'text-blue-500'
						} mb-6`}
					>
						{addProduct ? 'Add Product' : 'Edit Product'}
					</h1>
					<p
						className={`mb-1 text-sm font-semibold ${
							addProduct ? 'text-green-500' : 'text-blue-500'
						}`}
					>
						Product name
					</p>
					<div
						className={`${
							addProduct
								? 'border-green-500 hover:border-green-700'
								: 'border-blue-500 hover:border-blue-700'
						} px-4 py-2 border-2 rounded-lg hover:cursor-pointer mb-4`}
					>
						<input
							type="text"
							placeholder="Product name"
							value={name}
							name="name"
							onChange={handleChange}
							className="outline-none w-full"
							required
						/>
					</div>
					<p
						className={`mb-1 text-sm font-semibold ${
							addProduct ? 'text-green-500' : 'text-blue-500'
						}`}
					>
						Description
					</p>
					<div
						className={`${
							addProduct
								? 'border-green-500 hover:border-green-700'
								: 'border-blue-500 hover:border-blue-700'
						} px-4 py-2 border-2 rounded-lg hover:cursor-pointer mb-4`}
					>
						<input
							type="text"
							placeholder="Description"
							name="description"
							value={description}
							onChange={handleChange}
							className="outline-none w-full"
							required
						/>
					</div>
					<p
						className={`mb-1 text-sm font-semibold ${
							addProduct ? 'text-green-500' : 'text-blue-500'
						}`}
					>
						Price
					</p>
					<div
						className={`${
							addProduct
								? 'border-green-500 hover:border-green-700'
								: 'border-blue-500 hover:border-blue-700'
						} px-4 py-2 border-2 rounded-lg hover:cursor-pointer mb-4`}
					>
						<input
							type="number"
							placeholder="Price"
							name="price"
							value={price}
							onChange={handleChange}
							className="outline-none w-full"
							required
						/>
					</div>
					<p
						className={`mb-1 text-sm font-semibold ${
							addProduct ? 'text-green-500' : 'text-blue-500'
						}`}
					>
						Stocks
					</p>
					<div
						className={`${
							addProduct
								? 'border-green-500 hover:border-green-700'
								: 'border-blue-500 hover:border-blue-700'
						} px-4 py-2 border-2 rounded-lg hover:cursor-pointer mb-10`}
					>
						<input
							type="number"
							placeholder="Stocks"
							name="stocks"
							value={stocks}
							onChange={handleChange}
							className="outline-none w-full"
							required
						/>
					</div>
					<button
						type="submit"
						className={`px-4 py-2 text-white rounded-full ${
							loading
								? 'bg-gray-500 hover:cursor-not-allowed'
								: addProduct
								? 'bg-green-500 hover:bg-green-300'
								: 'bg-blue-500 hover:bg-blue-300'
						}`}
						disabled={loading}
					>
						{loading ? <LoopOutlinedIcon className="animate-spin" /> : 'Save'}
					</button>
				</form>
			);
		}
		return (
			<React.Fragment>
				<div className="w-full flex flex-col gap-2">
					<h1 className="text-2xl text-black font-semibold">{product.name}</h1>
					<p className="text-gray-500 text-sm">{product?.description}</p>
					<h2 className="text-xl text-black font-semibold">
						{formatPrice({
							price: product.price,
							format: 'PHP',
							withoutCents: true,
						})}
					</h2>
					<h3>
						Only{' '}
						<span className="font-semibold">
							{(product?.stocks || 0) - quantity}
						</span>{' '}
						left
					</h3>
				</div>
				<form
					className="flex flex-col item-center justify-center gap-4 w-full"
					onSubmit={(e) => submit(e)}
				>
					<div className="px-4 py-2 border-2 border-blue-500 rounded-lg hover:cursor-pointer hover:border-blue-700">
						<input
							type="number"
							placeholder="Quantity"
							min={0}
							max={parseInt(product?.stocks) || 0}
							value={quantity}
							onChange={(e) => setQuantity(e.target.value)}
							className="outline-none w-full"
						/>
					</div>
					<h1 className="text-black whitespace-nowrap">
						Your total amount is:{' '}
						<span className="font-semi-bold text-blue-500">
							{formatPrice({
								price: product.price * quantity,
								format: 'PHP',
								withoutCents: true,
							})}
						</span>
					</h1>

					{loading ? (
						<button
							type="submit"
							className="px-4 py-2 bg-gray-500 text-white rounded-full hover:cursor-not-allowed"
							disabled
						>
							<LoopOutlinedIcon className="animate-spin" />
						</button>
					) : (
						<button
							type="submit"
							className="px-4 py-2 bg-green-500 hover:bg-green-300 text-white rounded-full"
						>
							{isNil(user?.id) ? 'Login to continue' : 'Buy'}
						</button>
					)}
				</form>
			</React.Fragment>
		);
	}

	return (
		<div className="h-screen w-screen absolute bg-black bg-opacity-90 flex items-center justify-center z-50 fade-animation">
			<div
				className={`relative md:p-20 bg-white rounded-lg flex flex-row gap-6 ${
					user?.isAdmin ? 'md:p-6 py-5 px-6' : 'md:p-20 py-16 px-6'
				}`}
			>
				<CloseOutlinedIcon
					className="absolute text-black top-5 right-5 hover:cursor-pointer hover:text-blue-500"
					onClick={() => setShowProductModal(false)}
				/>
				{renderMainContent()}
			</div>
		</div>
	);
}
