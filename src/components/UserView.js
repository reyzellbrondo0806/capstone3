import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import UserContext from '../UserContext';
import { formatPrice } from '../utils/formatter';
import ArrowForwardIosOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined';
import ArrowBackIosOutlinedIcon from '@mui/icons-material/ArrowBackIosOutlined';
import { isEmpty } from 'lodash-es';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Loading from '../components/loaders/Loading';
import { API_BASE_ROUTE } from './constants/main';

export default function UserView() {
	const navigate = useNavigate();
	const { searchTermValue } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [loading, setLoading] = useState(true);
	const [currentPage, setCurrentPage] = useState(1);
	const [lastPage, setLastPage] = useState(0);

	useEffect(() => {
		if (isEmpty(searchTermValue)) {
			fetchActiveProducts();
		} else {
			fetchSearchedProduct();
		}
	}, [currentPage]);

	useEffect(() => {
		// Always set the current page to 1
		setCurrentPage(1);
	}, []);

	useEffect(() => {
		if (isEmpty(searchTermValue)) {
			setCurrentPage(1);
			fetchActiveProducts();
		} else {
			setCurrentPage(1);
			fetchSearchedProduct();
		}
	}, [searchTermValue]);

	function fetchSearchedProduct() {
		setLoading(true);
		axios
			.post(`${API_BASE_ROUTE}/products/searchByName?page=${currentPage}`, {
				name: searchTermValue,
			})
			.then((res) => {
				const { data } = res;
				setProducts(data.products);
				setLastPage(parseInt(data.totalPages));
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error occured',
					icon: 'error',
					text: 'Error occured while fetching products',
				});
			})
			.finally(() => {
				setLoading(false);
			});
	}

	function fetchActiveProducts() {
		setLoading(true);
		axios
			.get(`${API_BASE_ROUTE}/products/activeProducts?page=${currentPage}`)
			.then((res) => {
				const { data } = res;
				setProducts(data.products);
				setLastPage(parseInt(data.totalPages));
			})
			.catch((err) => {
				Swal.fire({
					title: 'Error occured',
					icon: 'error',
					text: 'Error occured while fetching products',
				});
			})
			.finally(() => {
				setLoading(false);
			});
	}

	if (loading) {
		return <Loading />;
	}

	function renderPaginationButtons() {
		return (
			<div className="w-full flex flex-row justify-between items-center py-4 px-3">
				<button
					className={`${
						currentPage === 1
							? 'text-gray-500 hover:cursor-not-allowed'
							: 'text-blue-700'
					}`}
					onClick={() => {
						if (currentPage !== 1) {
							setCurrentPage(currentPage - 1);
						}
					}}
				>
					<ArrowBackIosOutlinedIcon />
				</button>
				<button
					className={`${
						lastPage > currentPage
							? 'text-blue-700'
							: 'text-gray-500 hover:cursor-not-allowed'
					}`}
					onClick={() => {
						console.log(lastPage);
						console.log(currentPage);
						if (lastPage > currentPage) {
							setCurrentPage(currentPage + 1);
						}
					}}
				>
					<ArrowForwardIosOutlinedIcon />
				</button>
			</div>
		);
	}

	return (
		<section className="relative">
			<div className="w-screen flex flex-col">
				{products?.map((product) => {
					return (
						<div
							key={product._id}
							className="w-full flex flex-row gap-5 items-center justify-center px-10 py-4 border-b-2 border-gray-300"
						>
							<div className="text-center">
								<h1 className="text-2xl font-bold text-black">
									{product.name}
								</h1>
								<p className="text-base text-gray-500">
									{product.description}
									<br />
									<span className="font-semibold">
										Starts at{' '}
										{formatPrice({
											price: product.price,
											format: 'PHP',
											withoutCents: true,
										})}
									</span>
								</p>

								<button
									className="text-blue-700 px-6 py-3 border-2 border-blue-700 rounded-full hover:bg-blue-700 hover:text-white text-base mt-6"
									onClick={() => navigate(`/products/${product._id}`)}
								>
									Buy now
								</button>
							</div>
							<div>IMAGE</div>
						</div>
					);
				})}
			</div>
			{renderPaginationButtons()}
		</section>
	);
}
