import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/navigations/AppNavBar';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import './App.css';
import { UserProvider } from './UserContext';
import axios from 'axios';
import ProductModal from './components/modals/ProductModal';
import { API_BASE_ROUTE } from './components/constants/main';

function App() {
	// State hook for the user state that's defined here for a global scope
	// Initialized as an object with properties from the localStorage
	// This will be used to store the user information and will be used for validating if a user is logged in on the app or not

	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
		access: null,
	});

	const [searchTermValue, setSearchTermValue] = useState('');
	const [menuOpen, setMenuOpen] = useState(false);
	const [hideTopBar, setHideTopBar] = useState(false);
	const [product, setProduct] = useState({});
	const [showProductModal, setShowProductModal] = useState(false);
	const [showBuyButton, setShowBuyButton] = useState(false);
	const [productSaved, setProductSaved] = useState(false);
	const [userProducts, setUserProducts] = useState([]);

	// Function for clearing localStorage on logout
	const unsetUser = () => {
		localStorage.clear();
	};

	function fetchUserDetails() {
		const token = localStorage.getItem('token');
		if (!token) return;

		axios
			.get(`${API_BASE_ROUTE}/users/details`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((res) => {
				const { data } = res;
				setUser({
					...user,
					id: data._id,
					isAdmin: data.isAdmin,
				});
			})
			.catch(() => {
				console.log('Something went wrong');
			});
	}

	//Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
	useEffect(() => {
		fetchUserDetails();
	}, []);

	return (
		<UserProvider
			value={{
				user,
				setUser,
				unsetUser,
				searchTermValue,
				setSearchTermValue,
				menuOpen,
				setMenuOpen,
				hideTopBar,
				setHideTopBar,
				product,
				setProduct,
				showProductModal,
				setShowProductModal,
				setShowBuyButton,
				showBuyButton,
				productSaved,
				setProductSaved,
				setUserProducts,
				userProducts,
			}}
		>
			<Router>
				<div className="relative">
					{showProductModal ? <ProductModal /> : null}
					<AppNavbar />
					<Routes>
						<Route path="/" element={<Home />} />
						<Route path="/products" element={<Products />} />
						<Route path="/products/:productId" element={<ProductView />} />
						<Route path="/register" element={<Register />} />
						<Route path="/login" element={<Login />} />
						<Route path="/logout" element={<Logout />} />
						{/* 
					<Route path="/addProduct" element={<AddProduct />} />
					<Route path="/profile" element={<Profile />} /> */}
					</Routes>

					{showBuyButton ? (
						<div className="fixed bottom-0 left-1/2 transform -translate-x-1/2 w-full flex justify-center items-center py-6 border-t-2 border-gray-300 shadow-lg">
							<button
								className="bg-blue-500 px-6 py-4 text-white font-semibold text-base rounded-full hover:bg-blue-300 hover:cursor-pointer"
								onClick={() => setShowProductModal(true)}
							>
								Buy now
							</button>
						</div>
					) : null}
				</div>
			</Router>
		</UserProvider>
	);
}

export default App;
